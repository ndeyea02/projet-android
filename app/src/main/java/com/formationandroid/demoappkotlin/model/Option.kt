package com.formationandroid.demoappkotlin.model


import com.google.gson.annotations.SerializedName

data class Option(
    @SerializedName("id")
    val id: Int,
    @SerializedName("nom")
    val nom: String,
    @SerializedName("prix")
    val prix: Int
)