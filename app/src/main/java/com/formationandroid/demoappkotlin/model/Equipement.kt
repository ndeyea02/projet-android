package com.formationandroid.demoappkotlin.model


import com.google.gson.annotations.SerializedName

data class Equipement(
    @SerializedName("id")
    val id: Int,
    @SerializedName("nom")
    val nom: String
)