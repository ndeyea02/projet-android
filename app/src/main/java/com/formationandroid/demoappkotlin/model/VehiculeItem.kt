package com.formationandroid.demoappkotlin.model


import com.google.gson.annotations.SerializedName

data class VehiculeItem(
    @SerializedName("agemin")
    val agemin: Int,
    @SerializedName("categorieco2")
    val categorieco2: String,
    @SerializedName("disponible")
    val disponible: Int,
    @SerializedName("equipements")
    val equipements: List<Equipement>,
    @SerializedName("id")
    val id: Int,
    @SerializedName("image")
    val image: String,
    @SerializedName("nom")
    val nom: String,
    @SerializedName("options")
    val options: List<Option>,
    @SerializedName("prixjournalierbase")
    val prixjournalierbase: Int,
    @SerializedName("promotion")
    val promotion: Int
)