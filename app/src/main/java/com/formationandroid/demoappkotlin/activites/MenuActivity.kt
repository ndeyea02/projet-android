package com.formationandroid.demoappkotlin.activites

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.formationandroid.demoappkotlin.R

class MenuActivity : AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean
    {
        menuInflater.inflate(R.menu.demo_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        return when (item.itemId)
        {
            R.id.action_reglages ->
            {
                Log.i("tag", "réglages")
                true
            }
            R.id.action_favori ->
            {
                Log.i("tag", "favori")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}