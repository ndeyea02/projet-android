package com.formationandroid.demoappkotlin.activites

import android.icu.lang.UCharacter.GraphemeClusterBreak.V
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.formationandroid.demoappkotlin.R
import com.formationandroid.demoappkotlin.adapters.VehiculesAdapter
import com.formationandroid.demoappkotlin.bo.Vehicule
import com.formationandroid.demoappkotlin.metier.bdd.AppDatabaseHelper
import com.formationandroid.demoappkotlin.metier.dao.VehiculeDAO
import com.formationandroid.demoappkotlin.metier.dto.VehiculeDTO

class ListeVehiculesActivity : AppCompatActivity() {
    //private lateinit var coursesAdapter: CoursesAdapter
    //private lateinit var recyclerViewCourses: RecyclerView
    private lateinit var vehiculeAdapter: VehiculesAdapter
    private lateinit var recyclerViewVehicule: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_liste)

        // recyclerView :
        recyclerViewVehicule = findViewById(R.id.liste_vehicule)

        // à ajouter pour de meilleures performances :
        recyclerViewVehicule.setHasFixedSize(true)

        // layout manager, décrivant comment les items sont disposés :
        val layoutManager = LinearLayoutManager(this)
        recyclerViewVehicule.layoutManager = layoutManager

        // contenu :

        val listeVehiculesAdapter: List<VehiculeDAO> =
            AppDatabaseHelper.getDatabase(this).vehiculeDAO().getListeVehicule()

        // adapter :

        recyclerViewVehicule.adapter = vehiculeAdapter
    }

    fun clicAjouter(view: View) {
        val editTextVehicule: EditText = findViewById(R.id.liste_vehicule)

        // ajout :

       // AppDatabaseHelper.getDatabase(this).vehiculeDAO().insert(vehiculeTDO)

        // contenu :
        val listeVehicules: List<VehiculeDAO> =
            AppDatabaseHelper.getDatabase(this).vehiculeDAO().getListeVehicule()


        editTextVehicule.setText("")
        recyclerViewVehicule.smoothScrollToPosition(0)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.demo_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_reglages -> {
                Log.i("tag", "réglages")
                true
            }
            R.id.action_favori -> {
                Log.i("tag", "favori")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
