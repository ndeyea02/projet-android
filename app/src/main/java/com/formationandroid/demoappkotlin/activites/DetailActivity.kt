package com.formationandroid.demoappkotlin.activites

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentTransaction
import com.formationandroid.demoappkotlin.R
import com.formationandroid.demoappkotlin.fragments.DetailFragment
import com.formationandroid.demoappkotlin.fragments.EXTRA_VEHICULE
import com.formationandroid.demoappkotlin.metier.dto.VehiculeDTO

const val EXTRA_VEH_DETAIL = "detailVehicule"

class DetailActivity : AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        // argument envoyé depuis la liste :
        val vehicule: VehiculeDTO? = intent.getParcelableExtra(EXTRA_VEH_DETAIL)

        // fragment :
        val fragment = DetailFragment()

        // paramètres vers le fragment :
        val bundle = Bundle()
        bundle.putParcelable(EXTRA_VEHICULE, vehicule)
        fragment.arguments = bundle

        // transaction :
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.conteneur_fragment, fragment, "exemple2")
        transaction.commit()
    }

}