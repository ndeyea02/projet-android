package com.formationandroid.demoappkotlin.metier.dto

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "vehicule")
@Parcelize
data class VehiculeDTO
    (
    @PrimaryKey(autoGenerate = true)
    val vehiculeId: Long = 0,
    val nom: String? = null,
    val catégorie_CO2: String? = null,
    val prix: Float = 0f) : Parcelable
