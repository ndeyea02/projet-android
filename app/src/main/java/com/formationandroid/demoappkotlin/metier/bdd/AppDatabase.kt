package com.formationandroid.demoappkotlin.metier.bdd

import androidx.room.Database
import androidx.room.RoomDatabase
import com.formationandroid.demoappkotlin.metier.dao.VehiculeDAO


@Database(entities = [VehiculeDAO::class], version = 2)
abstract class AppDatabase : RoomDatabase()
{
    //abstract fun coursesDAO(): CoursesDAO
    abstract fun vehiculeDAO(): VehiculeDAO
}
