package com.formationandroid.demoappkotlin.metier.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import androidx.room.*
import com.formationandroid.demoappkotlin.metier.dto.VehiculeDTO
import com.formationandroid.demoappkotlin.model.Vehicule


abstract class VehiculeDAO
{
    @Query("SELECT * FROM vehicule ")
    abstract fun getListeVehicule(): List<VehiculeDAO>

    @Insert
    abstract fun insert(vararg  Vehicule: VehiculeDAO)

    @Update
    abstract fun update(vararg Vehicule: VehiculeDAO)

    @Delete
    abstract fun delete(vararg Vehicule: VehiculeDAO)
}