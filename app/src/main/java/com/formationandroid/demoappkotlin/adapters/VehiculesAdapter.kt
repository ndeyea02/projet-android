package com.formationandroid.demoappkotlin.adapters

import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.formationandroid.demoappkotlin.R
import com.formationandroid.demoappkotlin.activites.DetailActivity
import com.formationandroid.demoappkotlin.activites.EXTRA_VEH_DETAIL
import com.formationandroid.demoappkotlin.activites.ListeVehiculesActivity
import com.formationandroid.demoappkotlin.fragments.DetailFragment
import com.formationandroid.demoappkotlin.fragments.EXTRA_VEHICULE
import com.formationandroid.demoappkotlin.metier.dto.VehiculeDTO

class VehiculesAdapter (private var listeVehicule: MutableList<VehiculeDTO>,  private val listeActivity: ListeVehiculesActivity) :RecyclerView.Adapter<VehiculesAdapter.VehiculeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehiculeViewHolder {
        val viewVehicule = LayoutInflater.from(parent.context)
                                        .inflate(R.layout.item_vehicule, parent, false)
        return VehiculeViewHolder(viewVehicule)
    }

    override fun onBindViewHolder(holder: VehiculeViewHolder, position: Int) {
        holder.textViewListeVehicule.text= listeVehicule[position].nom
    }

    override fun getItemCount(): Int = listeVehicule.size

    inner class VehiculeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewListeVehicule: TextView = itemView.findViewById(R.id.liste_vehicule)

        init
        {
            itemView.setOnClickListener {
                val vehiculeDTO = listeVehicule[adapterPosition]
                Log.d(TAG, "vehicule: $vehiculeDTO")

                if (listeActivity.findViewById<FrameLayout>(R.id.conteneur_fragment) != null)
                {
                    // tablette :
                    val fragment = DetailFragment()

                    // paramètres vers le fragment :
                    val bundle = Bundle()
                    bundle.putParcelable(EXTRA_VEHICULE, vehiculeDTO)
                    fragment.arguments = bundle

                    // transaction :
                    val transaction: FragmentTransaction = listeActivity.supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.conteneur_fragment, fragment, "exemple2")
                    transaction.commit()
                }
                else
                {
                    // smartphone :
                    val intent = Intent(itemView.context, DetailActivity::class.java)
                    intent.putExtra(EXTRA_VEH_DETAIL, vehiculeDTO)
                    itemView.context.startActivity(intent)
                }
            }
        }
    }

}